package tac.home.jarvis.app.activities;

import tac.home.jarvis.app.R;
import tac.home.jarvis.app.util.Client;
import tac.home.jarvis.app.util.SystemUiHider;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;

import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class HomeActivity extends tac.home.jarvis.app.util.View implements View.OnClickListener {

    private static final String TAG = "HomeController";
    private static final String IP_SERVER = "192.168.0.11";
    private Client client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        final Button buttonLightLivingRoom = (Button) findViewById(R.id.button_light_living_room);
        final Button buttonTelevision = (Button) findViewById(R.id.button_television);

        buttonLightLivingRoom.setOnClickListener(this);
        buttonTelevision.setOnClickListener(this);

        //Check if Recognizer is available
        if ( false == isAvailable(this, new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)) ){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,   Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.voicesearch&hl=fr"));
            this.startActivity(browserIntent);
        }
        //TTS.init(this.activity, null, new VoiceCompleted());
        //Connection with server
        this.client = new Client(this, IP_SERVER, 4444);

    }


    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_light_living_room:
                this.client.sendCommand("1");
                break;
            case R.id.button_television:
                Intent intent = new Intent(this, TelevisionActivity.class);
                this.startActivity(intent);
                break;
        }

    }

    public static boolean isAvailable(Context ctx, Intent intent) {

        final PackageManager mgr = ctx.getPackageManager();

        List<ResolveInfo> list = mgr.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        return list.size() > 0;

    }

}
