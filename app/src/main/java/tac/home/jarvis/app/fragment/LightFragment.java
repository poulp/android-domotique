package tac.home.jarvis.app.fragment;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import tac.home.jarvis.app.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@EFragment
public class LightFragment extends Fragment{

    @ViewById
    Button buttonSaloon;

    @AfterViews
    public void setListener(){
        buttonSaloon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Domotique","Light salon");
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_light, container, false);
    }
}
