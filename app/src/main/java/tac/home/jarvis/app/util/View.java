package tac.home.jarvis.app.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import tac.home.jarvis.app.R;
import tac.home.jarvis.app.activities.AppPreferenceActivity;

/**
 * Created by chakour on 07/05/14.
 */
public abstract class View extends Activity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.option:
                Intent i = new Intent(this, AppPreferenceActivity.class);
                this.startActivity(i);
               break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
