package tac.home.jarvis.app.nav;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import tac.home.jarvis.app.R;

@EViewGroup(R.layout.nav_item)
public class NavItemView extends LinearLayout{

    @ViewById
    TextView navTitle;

    public NavItemView(Context context){
        super(context);
    }

    public void bind(NavItem item, int position) {
        this.navTitle.setText(item.getTitle());
    }
}
