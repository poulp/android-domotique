package tac.home.jarvis.app.nav;

public class NavItem {

    private String title;

    public NavItem(){}

    public NavItem(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }
}
