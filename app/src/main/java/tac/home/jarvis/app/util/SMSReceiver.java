package tac.home.jarvis.app.util;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.telephony.SmsMessage;
import android.widget.Toast;

import tac.home.jarvis.app.activities.ReadSMSActivity;

import static android.widget.Toast.*;

/**
 * Created by chakour on 10/05/14.
 */
public class SMSReceiver extends BroadcastReceiver {

    private final String ACTION_RECEIVE_SMS	= "android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(ACTION_RECEIVE_SMS)) {

            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");

                final SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                if (messages.length > -1) {

                    final String messageBody = messages[0].getMessageBody();
                    final String phoneNumber = messages[0].getDisplayOriginatingAddress();

                    Intent i = new Intent(context, ReadSMSActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("phoneNumber", phoneNumber);
                    i.putExtra("messageBody", messageBody);
                    context.startActivity(i);

                }

            }
        }

    }



}
