package tac.home.jarvis.app.nav;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

public class NavListAdapter extends BaseAdapter {

    private ArrayList<NavItem> navDrawerItems;
    Context context;

    public NavListAdapter(Context context, ArrayList<NavItem> items) {
        this.context = context;
        this.navDrawerItems = items;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        NavItemView mNavDrawerItemView;
        if(view == null) {
            mNavDrawerItemView = NavItemView_.build(context);
        } else {
            mNavDrawerItemView = (NavItemView) view;
        }
        mNavDrawerItemView.bind(navDrawerItems.get(position), position);
        return mNavDrawerItemView;
    }

}
