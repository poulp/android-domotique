package tac.home.jarvis.app.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import tac.home.jarvis.app.R;
import tac.home.jarvis.app.util.View;

public class ReadSMSActivity extends View implements TextToSpeech.OnUtteranceCompletedListener {

    private final static String TAG = "ReadSmsController";
    private static String messageBody;
    private static String phoneNumber;
    private static String senderName;
    private TextToSpeech tts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recognition);

        final ReadSMSActivity activity = this;
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {

                    int result = tts.setLanguage(Locale.FRANCE);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e(TAG, "This Language is not supported");
                    } else {
                        tts.setOnUtteranceCompletedListener(activity);
                        final Bundle bundle = getIntent().getExtras();
                        phoneNumber = bundle.getString("phoneNumber");
                        messageBody = bundle.getString("messageBody");
                        senderName = getContactName(activity, phoneNumber);

                        if(senderName == null)
                            tts.speak("Vous avez un nouveau message d'un inconnu", TextToSpeech.QUEUE_ADD, null);
                        else
                            tts.speak("Vous avez un nouveau message de " + senderName, TextToSpeech.QUEUE_ADD, null);

                        HashMap<String, String> h = new HashMap<String, String>();
                        h.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "read_message");
                        tts.speak("Faut-il lire le message ?", TextToSpeech.QUEUE_ADD, h);
                    }

                } else {
                    Log.e(TAG, "Initilization Failed!");
                }
            }

        });

    }


    private static String getContactName(Context context, String phoneNumber) {

        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1 && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            for ( String s : matches) {
                if ( s.contains("ué") || s.contains("ui") ) {
                    Toast t = Toast.makeText(this, messageBody, Toast.LENGTH_LONG);
                    t.show();
                    tts.speak(messageBody, TextToSpeech.QUEUE_ADD, null);
                    return;
                }
            }
        }

    }

    @Override
    public void onUtteranceCompleted(String utteranceId) {

        if (utteranceId.equals("read_message")) {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak");
            this.startActivityForResult(intent, 1);
        }

    }

}
