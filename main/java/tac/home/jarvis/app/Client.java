package tac.home.jarvis.app;

import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;


/**
 * Created by chakour on 01/05/14.
 */
public class Client extends Thread {

    private String ip;
    private int port;
    private Socket client;
    private HomeActivity activity;
    private Receiver receiver;

    public Client ( HomeActivity activity, String ip, int port ) {

        this.ip = ip;
        this.port = port;
        this.activity = activity;
        this.start();

    }


    public void run() {

        Log.i("Client", "Client is starting");
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast t = Toast.makeText(activity, "Starting client", Toast.LENGTH_LONG);
                t.show();
            }
        });

        try {
            this.client = new Socket(this.ip, this.port);  //connect to server
            this.receiver = new Receiver(this.activity, this.client);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendCommand(String command) {

        try {
            this._sendTo(command, this.client);
        }catch (Exception e) {

        }

    }


    private void _sendTo( String message, Socket socket ) throws IOException, ClassNotFoundException {

        PrintStream out = new PrintStream(socket.getOutputStream());
        // Write
        out.println(message);
        //out.close();

    }


    public void close() {

        try {
            this.client.close();
        }catch ( Exception e ) {

        }

    }
}
