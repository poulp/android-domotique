package tac.home.jarvis.app;

import tac.home.jarvis.app.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class HomeActivity extends Activity {

    private static final String IP_SERVER = "192.168.0.13";
    private Receiver com;
    private Client client;
    private TextToSpeech tts;


    private static int TTS_DATA_CHECK = 1;

    private void confirmTTSData() {
        Intent intent = new Intent(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(intent, TTS_DATA_CHECK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       /* this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        super.onCreate(savedInstanceState);

        this.tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    Toast t = Toast.makeText(getApplicationContext(), "TTS is running", Toast.LENGTH_SHORT);
                    t.show();
                }
                else {
                    //Handle initialization error here
                }
            }
        });

        setContentView(R.layout.activity_fullscreen);

        final Button buttonLightLivingRoom = (Button) findViewById(R.id.button_light_living_room);
        final Button buttonTelevision = (Button) findViewById(R.id.button_television);

        this.client = new Client(this, IP_SERVER, 4444);

        buttonLightLivingRoom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                client.sendCommand("light_living_room");

            }



        });

        buttonTelevision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), TelevisionActivity.class);
                startActivity(intent);

            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TTS_DATA_CHECK) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //Voice data exists
            }
            else {
                Intent installIntent = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    public void speak( String text ) {

        this.tts.speak(text, TextToSpeech.QUEUE_ADD, null);

    }

}
