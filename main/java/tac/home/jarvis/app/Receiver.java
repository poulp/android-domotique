package tac.home.jarvis.app;

import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by chakour on 01/05/14.
 */
public class Receiver extends Thread{

    private Socket client;
    private HomeActivity activity;
    private String response;

    public Receiver( HomeActivity activity, Socket client ) {
        this.client = client;
        this.activity = activity;
        this.start();
    }

    public void run() {

        while ( true ) {

            try {


                this.response = this._receiveFrom(this.client);

                this.activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast t = Toast.makeText(activity, "Jarvis said "+response, Toast.LENGTH_LONG);
                        t.show();
                    }
                });

                this.activity.speak(this.response);

            }catch(Exception e) {

            }
        }
    }

    /**
     * Reception d'une donnée du serveur en respectant
     *
     * @param socket
     *
     * @throws IOException
     * @throws ClassNotFoundException
     *
     * @return
     *
     */

    private String _receiveFrom(Socket socket) throws IOException, ClassNotFoundException {

        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        String input = _read(in);

        return input;

    }

    /**
     * Lecture d'une ligne dans le buffer in
     *
     * @param in
     *
     * @throws java.io.IOException
     *
     * @return String
     *
     */

    private String _read( BufferedReader in ) throws IOException {

        String tmp = "";
        int t;
        do {t = in.read() ; tmp = tmp + (char)t; }
        while (t!=10) ;

        return tmp.substring( 0, tmp.length()-1 );

    }
}
